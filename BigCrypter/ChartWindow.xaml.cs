﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Controls.DataVisualization.Charting;
using System.Collections.ObjectModel;

namespace BigCrypter
{
    /// <summary>
    /// Логика взаимодействия для ChartWindow.xaml
    /// </summary>
    public partial class ChartWindow : Window
    {
        public ChartWindow()
        {
            InitializeComponent();
        }

        // Добавление графиков на окно
        public void add_chart()
        {
            MyLineSeries NewChart = new MyLineSeries();
            // Добавляем нулевую точку
            NewChart.ChartData = new ObservableCollection<ChartPoint> {
                new ChartPoint{ Value = 0, Time = DateTime.Now },
            };
            NewChart.DependentValuePath = "Value";
            NewChart.IndependentValuePath = "Time";
            NewChart.Title = "Клиент " + (Charts.Series.Count + 1);
            NewChart.ItemsSource = NewChart.ChartData;
            Charts.Series.Add(NewChart);
        }

        // Редактирование графиков
        public void edit_chart(int index_client)
        {
            // Добавляем точку к имеющейся серии
            MyLineSeries EditChart = (MyLineSeries)Charts.Series[index_client];
            EditChart.ChartData.Add(new ChartPoint { Value = EditChart.ChartData.Count, Time = DateTime.Now });
            EditChart.ItemsSource = EditChart.ChartData;
        }
    }

    // Переопределяем линию графика
    public class MyLineSeries : LineSeries
    {
        // Хранилище точек
        public ObservableCollection<ChartPoint> ChartData;
        public MyLineSeries()
        {
            ChartData = new ObservableCollection<ChartPoint> { };
            // Синхронизируем коллекцию с отрисовщиком
            ItemsSource = ChartData;
        }
    }

    // Точка графика
    public class ChartPoint
    {
        // Количество пакетов
        public int Value { get; set; }
        // Время
        public DateTime Time { get; set; }
    }
}
