﻿using System;
using System.Windows;
using System.Threading;
using System.ServiceModel;

namespace BigCrypter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private byte[] key = { 
                         0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2, 3,
                         0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2, 3
                     };

        private System.IO.FileInfo pathIn, pathOut;
        private System.IO.DirectoryInfo pathInDir, pathOutDir;
        private float blockSizeMultCore = 1024 * 1024 * 10 * System.Environment.ProcessorCount;

        private ServiceHost cryptoService;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void FocusFile(string file)
        {
            try
            {
                System.Diagnostics.Process.Start("explorer.exe", @"/select, " + file);
            }
            catch (Exception exc) { }
        }

        private void BlockGUI()
        {
            btnDecodeDir.IsEnabled = false;
            btnDecodeFile.IsEnabled = false;
            btnEncodeDir.IsEnabled = false;
            btnEncodeFile.IsEnabled = false;
            btnStartService.IsEnabled = false;
            btnStopService.IsEnabled = false;
            myPrBarText.Visibility = System.Windows.Visibility.Visible;
        }

        private void UnblockGUI()
        {
            btnDecodeDir.IsEnabled = true;
            btnDecodeFile.IsEnabled = true;
            btnEncodeDir.IsEnabled = true;
            btnEncodeFile.IsEnabled = true;
            if (cryptoService != null && CommunicationState.Opened == cryptoService.State)
            {
                btnStopService.IsEnabled = true;
                btnStartService.IsEnabled = false;
            }
            else
            {
                btnStopService.IsEnabled = false;
                btnStartService.IsEnabled = true;
            }
            myPrBar.Value = 0;
            myPrBarText.Visibility = System.Windows.Visibility.Hidden;
        }

        private async void ButtonEncodeFile_Click(object sender, RoutedEventArgs e)
        {
            BlockGUI();

            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.Multiselect = false;
            dlg.Title = "Выбрать файл для шифрования";
            dlg.CheckFileExists = true;

            if (dlg.ShowDialog().Value)
            {
                pathIn = new System.IO.FileInfo(dlg.FileName);
                pathOut = new System.IO.FileInfo(dlg.FileName + ".ikr");
                int blockNumbers = (int)Math.Ceiling((float)pathIn.Length / blockSizeMultCore);

                DateTime startTime = DateTime.Now;
                if (cryptoService == null || cryptoService.State != CommunicationState.Opened
                    || ((CryptoServiceLib.CryptoService)cryptoService.SingletonInstance).ClientsCount == 0)
                {
                    CryptoProvider.myEvent.Event += new CryptoProvider.endDelegate(delegate(object s, CryptoProvider.CryptoEventArgs arg) {
                        Dispatcher.BeginInvoke(new ThreadStart(delegate { myPrBar.Value = ((float)arg.indexBlock / (float)blockNumbers) * 100; }));
                    });
                    await System.Threading.Tasks.Task.Factory.StartNew(
                        () => CryptoProvider.encodeFileAsyncParallel(pathIn, pathOut, key)
                    );
                }
                else
                {
                    // Открываем окошко для графика
                    ChartWindow newWindow = new ChartWindow();
                    newWindow.Show();

                    await System.Threading.Tasks.Task.Factory.StartNew(
                        () => {
                            Dispatcher.BeginInvoke(new ThreadStart(delegate { myPrBarText.Visibility = System.Windows.Visibility.Hidden; }));
                            Dispatcher.BeginInvoke(new ThreadStart(delegate { myPrBar.IsIndeterminate = true; }));
                            var service = (CryptoServiceLib.CryptoService)cryptoService.SingletonInstance;

                            Dispatcher.BeginInvoke(new ThreadStart(delegate {
                                int a = service.ClientsCount;
                                while (a != 0)
                                {
                                    newWindow.add_chart();
                                    a--;
                                }
                            }));

                            service.EndEvent += new CryptoServiceLib.endDelegate(delegate(object s, CryptoServiceLib.CryptoEventArgs2 arg)
                            {
                                Dispatcher.BeginInvoke(new ThreadStart(delegate { newWindow.edit_chart(arg.indexBlock); }));
                            });

                            service.EncodeFile(pathIn, pathOut, key);
                            Dispatcher.BeginInvoke(new ThreadStart(delegate { myPrBar.IsIndeterminate = false; }));
                        }
                    );
                }
                DateTime endTime = DateTime.Now;
                MessageBox.Show("Шифрование завершено за " + (endTime - startTime).ToString() + "!");
                FocusFile(pathOut.FullName);
            }
            UnblockGUI();
        }

        private async void ButtonDecodeFile_Click(object sender, RoutedEventArgs e)
        {
            BlockGUI();

            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.Multiselect = false;

            dlg.Title = "Выбрать файл для дешифрования";
            dlg.CheckFileExists = true;
            dlg.Filter = "BigCrypt files (*.ikr) | *.ikr";
            if (dlg.ShowDialog().Value)
            {
                pathIn = new System.IO.FileInfo(dlg.FileName);
                int blockNumbers = (int)Math.Ceiling((float)pathIn.Length / blockSizeMultCore);

                // Переформировывание имени файла
                dlg.FileName = dlg.FileName.Substring(0, dlg.FileName.Length - 4);
                string[] arr = dlg.FileName.Split('/');
                arr[arr.Length - 1] = "[Decode version] - " + dlg.SafeFileName;
                dlg.FileName = "";
                foreach (string one in arr) {
                    dlg.FileName += one;
                }

                pathOut = new System.IO.FileInfo(dlg.FileName);

                DateTime startTime = DateTime.Now;

                if (cryptoService == null || cryptoService.State != CommunicationState.Opened
                    || ((CryptoServiceLib.CryptoService)cryptoService.SingletonInstance).ClientsCount == 0)
                {
                    CryptoProvider.myEvent.Event += new CryptoProvider.endDelegate(delegate(object s, CryptoProvider.CryptoEventArgs arg)
                    {
                        Dispatcher.BeginInvoke(new ThreadStart(delegate { myPrBar.Value = ((float)arg.indexBlock / (float)blockNumbers) * 100; }));
                    });
                    await System.Threading.Tasks.Task.Factory.StartNew(
                        () => CryptoProvider.decodeFileAsyncParallel(pathIn, pathOut, key)
                    );
                }
                else
                {
                    // Открываем окошко для графика
                    ChartWindow newWindow = new ChartWindow();
                    newWindow.Show();

                    await System.Threading.Tasks.Task.Factory.StartNew(
                        () => {
                            Dispatcher.BeginInvoke(new ThreadStart(delegate { myPrBarText.Visibility = System.Windows.Visibility.Hidden; }));
                            Dispatcher.BeginInvoke(new ThreadStart(delegate { myPrBar.IsIndeterminate = true; }));
                            var service = (CryptoServiceLib.CryptoService)cryptoService.SingletonInstance;

                            Dispatcher.BeginInvoke(new ThreadStart(delegate
                            {
                                int a = service.ClientsCount;
                                while (a != 0)
                                {
                                    newWindow.add_chart();
                                    a--;
                                }
                            }));

                            service.EndEvent += new CryptoServiceLib.endDelegate(delegate(object s, CryptoServiceLib.CryptoEventArgs2 arg)
                            {
                                Dispatcher.BeginInvoke(new ThreadStart(delegate { newWindow.edit_chart(arg.indexBlock); }));
                            });


                            service.DecodeFile(pathIn, pathOut, key);
                            Dispatcher.BeginInvoke(new ThreadStart(delegate { myPrBar.IsIndeterminate = false; }));
                        }
                   );
                }

                DateTime endTime = DateTime.Now;
                MessageBox.Show("Дешифрование завершено за " + (endTime - startTime).ToString() + "!");
                FocusFile(pathOut.FullName);
            }

            UnblockGUI();
        }

        private async void ButtonEncodeDir_Click(object sender, RoutedEventArgs e)
        {
            BlockGUI();
            System.Windows.Forms.FolderBrowserDialog dlg = new System.Windows.Forms.FolderBrowserDialog();
            dlg.Description = "Выбрать директорию для шифрования";
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                pathInDir = new System.IO.DirectoryInfo(dlg.SelectedPath);
                pathOut = new System.IO.FileInfo(dlg.SelectedPath + ".dikr");
                try
                {
                    DateTime startTime = DateTime.Now;
                    await System.Threading.Tasks.Task.Factory.StartNew(
                        () => CryptoProvider.encodeDir(pathInDir, pathOut, key)
                    );
                    DateTime endTime = DateTime.Now;
                    MessageBox.Show("Шифрование завершено за " + (endTime - startTime).ToString() + "!");
                    FocusFile(pathOut.FullName);
                }
                catch (System.IO.IOException ex)
                {
                    MessageBox.Show("Произошла ошибка ввода/вывода! " + ex.Message);
                }
                catch (System.UnauthorizedAccessException)
                {
                    MessageBox.Show("Нет доступа для записи в указанную директорию!");
                }
            }
            UnblockGUI();
        }

        private void BackgroundEncodeDir()
        {
            try
            {
                DateTime startTime = DateTime.Now;
                CryptoProvider.encodeDir(pathInDir, pathOut, key);
                DateTime endTime = DateTime.Now;
                Dispatcher.BeginInvoke(new ThreadStart(delegate { myPrBar.IsIndeterminate = false; }));
                MessageBox.Show("Шифрование завершено за " + (endTime - startTime).ToString() + "!");
            }
            catch (System.IO.IOException ex)
            {
                Dispatcher.BeginInvoke(new ThreadStart(delegate { myPrBar.IsIndeterminate = false; }));  
                MessageBox.Show("Произошла ошибка ввода/вывода! " + ex.Message);
            }
            catch (System.UnauthorizedAccessException)
            {
                Dispatcher.BeginInvoke(new ThreadStart(delegate { myPrBar.IsIndeterminate = false; }));  
                MessageBox.Show("Нет доступа для записи в указанную директорию!");
            }
        }

        private async void ButtonDecodeDir_Click(object sender, RoutedEventArgs e)
        {
            BlockGUI();
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.Multiselect = false;
            dlg.Title = "Выбрать файл для дешифрования";
            dlg.CheckFileExists = true;
            dlg.Filter = "BigCryptDirectory files (*.dikr) | *.dikr";
            if (dlg.ShowDialog().Value)
            {
                pathIn = new System.IO.FileInfo(dlg.FileName);

                System.Windows.Forms.FolderBrowserDialog dirDlg = new System.Windows.Forms.FolderBrowserDialog();
                dirDlg.Description = "Выбрать выходную директорию";
                if (dirDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    pathOutDir = new System.IO.DirectoryInfo(dirDlg.SelectedPath);
                    myPrBar.IsIndeterminate = true;
                    try
                    {
                        DateTime startTime = DateTime.Now;
                        await System.Threading.Tasks.Task.Factory.StartNew(
                            () => CryptoProvider.decodeDir(pathIn, pathOutDir, key)
                        );
                        DateTime endTime = DateTime.Now;
                        myPrBar.IsIndeterminate = false;
                        MessageBox.Show("Дешифрование завершено за " + (endTime - startTime).ToString() + "!");
                    }
                    catch (System.IO.IOException ex)
                    {
                        myPrBar.IsIndeterminate = false;
                        MessageBox.Show("Произошла ошибка ввода/вывода! " + ex.Message);
                    }
                    catch (System.IO.InvalidDataException)
                    {
                        myPrBar.IsIndeterminate = false;
                        MessageBox.Show("Указанный файл не является архивом!");
                    }
                }
            }
            UnblockGUI();
        }

        private void BtnStartService_Click(object sender, RoutedEventArgs e)
        {         
            btnStartService.IsEnabled = false;
            btnStopService.IsEnabled = true;
            var service = new CryptoServiceLib.CryptoService();
            service.LogEvent += new CryptoServiceLib.LogDelegate(delegate(object s, CryptoServiceLib.CryptoEventArgs args) { Dispatcher.BeginInvoke(new ThreadStart(delegate { txtblckLog.Text += args.Text + "\n"; })); } );
            cryptoService = new ServiceHost(service, new Uri("net.tcp://localhost:8080/CryptoService"));
            cryptoService.Open();
            txtblckLog.Text += "Служба запущена.\n";
        }

        private void BtnStopService_Click(object sender, RoutedEventArgs e)
        {
            btnStartService.IsEnabled = true;
            btnStopService.IsEnabled = false;
            var service = (CryptoServiceLib.CryptoService)cryptoService.SingletonInstance;
            service.StopService();
            cryptoService.Close();
            cryptoService = null;
            txtblckLog.Text += "Служба остановлена.\n";
        }
    }
}