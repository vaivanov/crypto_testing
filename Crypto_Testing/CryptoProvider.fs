﻿module CryptoProvider

open System
open System.IO
open System.Security

/// Соединяет файлы в директории в один единый.
let private joinFiles (dirPath : DirectoryInfo) (destFile : FileInfo) =
    Compression.ZipFile.CreateFromDirectory(dirPath.FullName, destFile.FullName, Compression.CompressionLevel.NoCompression, false)

/// Разбивает скреплённые файлы на исходные.
let private splitFile (srcFile : FileInfo) (destDir : DirectoryInfo) =
    Compression.ZipFile.ExtractToDirectory(srcFile.FullName, destDir.FullName)

/// Десять мегабайт.
let private blockSize = 1024*1024*10

/// Количество ядер
let private core = System.Environment.ProcessorCount

/// Переопредляем параметр события
type CryptoEventArgs(indexBlock : int) =
    inherit EventArgs()    
    member this.indexBlock = indexBlock

/// Делегат - обработчик события
type endDelegate = delegate of obj * CryptoEventArgs -> unit

/// Инициализация события
type EventEndBlock() =
    let event = new Event<endDelegate, CryptoEventArgs>()
    [<CLIEvent>]
    member this.Event = event.Publish
    member this.GetEvent(arg) =
        event.Trigger(this, new CryptoEventArgs(arg))

/// Событие оповещающее о завершении обработки одного блока
let myEvent = new EventEndBlock()

// Инициализация AES
let initialAES key =
    let aes256 = new Cryptography.RijndaelManaged()
    aes256.KeySize <- 256
    aes256.BlockSize <- 256
    aes256.Padding <- Cryptography.PaddingMode.Zeros
    aes256.Mode <- Cryptography.CipherMode.ECB
    aes256.Key <- key
    aes256.GenerateIV()
    aes256

/// Основной процесс параллельного преобразования.
let uniFileParallel (fileIn : FileInfo) (fileOut : FileInfo) (cryptTool : Cryptography.ICryptoTransform) =
    let block = Array.zeroCreate (blockSize * core)
    let encodeBlock = Array.zeroCreate (blockSize * core)
    use fileInStream = fileIn.OpenRead()
    use fileOutStream = fileOut.OpenWrite()
    let asyncEncode realRead = 
        let realCore = if realRead = blockSize * core then core else int (ceil <| (/) (float realRead) (float blockSize))
        let tasks  = seq {
            for i in 1 .. realCore -> async {
                if realRead - (blockSize * i) >= 0 then
                    lock encodeBlock (fun _ -> cryptTool.TransformBlock(block, (i-1)*blockSize, blockSize, encodeBlock, (i-1)*blockSize)) |> ignore
                    return blockSize                    
                else
                    let count = abs <| blockSize * (i-1) - realRead
                    let result = cryptTool.TransformFinalBlock(block, (i-1)*blockSize, count)
                    lock encodeBlock (fun _ -> Array.blit result 0 encodeBlock ((i-1)*blockSize) result.Length)
                    return result.Length
            }
        }
        Async.RunSynchronously(Async.Parallel tasks)
    let mutable index = 1
    while fileInStream.Length - fileInStream.Position <> 0L do
        let realRead = fileInStream.Read(block, 0, blockSize * core)
        let realTransformed = realRead |> asyncEncode |> Array.sum
        fileOutStream.Write(encodeBlock, 0, realTransformed)
        myEvent.GetEvent(index)
        index <- index + 1
    fileOutStream.Flush()

/// Непараллельная версия основного преобразования.  
let uniFileSync (fileIn : FileInfo) (fileOut : FileInfo) (cryptTool : Cryptography.ICryptoTransform) =
    let block = Array.zeroCreate blockSize
    let enBlock = Array.zeroCreate blockSize
      
    use fileInStream = fileIn.OpenRead()
    use fileOutStream = fileOut.OpenWrite()

    // криптопоток свзяываем на запись с потоком выходного файла
    let cs = new Cryptography.CryptoStream(fileOutStream, cryptTool, Cryptography.CryptoStreamMode.Write)
   
    // пока есть что брать из файла, берём и кодируем
    while fileInStream.Length - fileInStream.Position <> 0L do
        let startTime1 = DateTime.Now 
        let realRead = fileInStream.Read(block, 0, blockSize)
        let time1 = DateTime.Now - startTime1 
        printfn "%A" time1
        let startTime = DateTime.Now  
        cs.Write(block, 0, realRead)
        let time = DateTime.Now - startTime
        printfn "%A" time

    cs.FlushFinalBlock()

let shift(b, shiftAmount) = 
    let num = int(b) - 0
    let offsetNum = (num + shiftAmount) % 256
    let result = offsetNum + 0
    if offsetNum < 0 then
        byte(255 + offsetNum + 1)
    else
        byte(offsetNum + 0)

let encrypt(bytes:byte[], shiftAmount) =
    bytes |> Array.map (fun b -> shift(b, shiftAmount)) 

let decrypt(bytes:byte[], shiftAmount) =
    bytes |> Array.map (fun b -> shift(b, 256-shiftAmount))

/// Основной процесс параллельного преобразования.
let uni (fileIn : FileInfo) (fileOut : FileInfo) func =
    let block = Array.zeroCreate (blockSize * core)
    let encodeBlock = Array.zeroCreate (blockSize * core)
    use fileInStream = fileIn.OpenRead()
    use fileOutStream = fileOut.OpenWrite()
    let asyncEncode realRead = 
        let realCore = if realRead = blockSize * core then core else int (ceil <| (/) (float realRead) (float blockSize))
        let tasks  = seq {
            for i in 1 .. realCore -> async {
                if realRead - (blockSize * i) >= 0 then
                    lock encodeBlock (fun _ -> 
                                        let result = func(block.[(i-1)*blockSize..i*blockSize-1], 16)
                                        Array.blit  result 0 encodeBlock ((i-1)*blockSize) result.Length
                                    )
                    return blockSize                    
                else
                    lock encodeBlock (fun _ -> 
                                        let result = func(block.[(i-1)*blockSize..], 16)
                                        Array.blit  result 0 encodeBlock ((i-1)*blockSize) result.Length
                                    )
                    return abs <| blockSize * (i-1) - realRead
            }
        }
        Async.RunSynchronously(Async.Parallel tasks)
    let mutable index = 1
    while fileInStream.Length - fileInStream.Position <> 0L do
        let realRead = fileInStream.Read(block, 0, blockSize * core)
        let realTransformed = realRead |> asyncEncode |> Array.sum
        fileOutStream.Write(encodeBlock, 0, realTransformed)
        myEvent.GetEvent(index)
        index <- index + 1
    fileOutStream.Flush()

/// Параллельная версия шифрования файла.
let encodeFileAsyncParallel (fileIn : FileInfo) (fileOut : FileInfo) key = 
    uni fileIn fileOut encrypt
(*
    let aes256 = initialAES key
    let encryptor = aes256.CreateEncryptor()
    uniFileParallel fileIn fileOut encryptor
*)

/// Параллельная версия дешифрования файла.
let decodeFileAsyncParallel (fileIn : FileInfo) (fileOut : FileInfo) key = 
    uni fileIn fileOut decrypt
(* 
    let aes256 = initialAES key
    let decryptor = aes256.CreateDecryptor()
    uniFileParallel fileIn fileOut decryptor
 *)

/// Непараллельная версия шифрования файла.
let encodeFileSync (fileIn : FileInfo) (fileOut : FileInfo) key = 
    let aes256 = initialAES key
    let encryptor = aes256.CreateEncryptor()
    uniFileSync fileIn fileOut encryptor


/// Непараллельная версия дешифрования файла
let decodeFileSync (fileIn : FileInfo) (fileOut : FileInfo) key =     
    let aes256 = initialAES key
    let decryptor = aes256.CreateDecryptor()
    uniFileSync fileIn fileOut decryptor

/// Шифрует файлы в указанной директории.
let encodeDir (dirIn : DirectoryInfo) (fileOut : FileInfo) key =
    let newInFile = new FileInfo(Path.Combine(dirIn.Parent.FullName, Guid.NewGuid().ToString()))
    joinFiles dirIn newInFile
    uni newInFile fileOut encrypt
    newInFile.Delete()

/// Дешифрует указанный файл и разбивает его на исходные файлы.
let decodeDir (fileIn : FileInfo) (dirOut : DirectoryInfo) key =
    let newOutFile = new FileInfo(Path.Combine(fileIn.DirectoryName + Guid.NewGuid().ToString()))
    uni fileIn newOutFile decrypt
    splitFile newOutFile dirOut
    newOutFile.Delete()
