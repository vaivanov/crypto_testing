﻿module ClientService

open System
open System.Security
open System.Threading.Tasks

open CryptoServiceLib

type ServiceStatus =
    Normal
    | Stopped

/// Криптослужба клиента.
type ClientService() =
    let mutable isBusy = false
    let mutable cipherKey : byte[] = null
    let mutable log = printfn "%s"
    let mutable aes : Cryptography.RijndaelManaged = null
    let mutable status = Normal

    /// Десять мегабайт.
    let blockSize = 1024*1024*10

    /// Количество ядер
    let cores = System.Environment.ProcessorCount

    let shift(b, shiftAmount) = 
        let num = int(b) - 0
        let offsetNum = (num + shiftAmount) % 256
        let result = offsetNum + 0
        if offsetNum < 0 then
            byte(255 + offsetNum + 1)
        else
            byte(offsetNum + 0)

    let encrypt(bytes:byte[], shiftAmount) =
        bytes |> Array.map (fun b -> shift(b, shiftAmount)) 

    let decrypt(bytes:byte[], shiftAmount) =
        bytes |> Array.map (fun b -> shift(b, 256-shiftAmount))

    /// Инициализация AES
    let initialAES key =
        let aes256 = new Cryptography.RijndaelManaged()
        aes256.KeySize <- 256
        aes256.BlockSize <- 256
        aes256.Padding <- Cryptography.PaddingMode.Zeros
        aes256.Mode <- Cryptography.CipherMode.ECB
        aes256.Key <- key
        aes256.GenerateIV()
        aes256

    /// Основной процесс параллельного преобразования.
    let uniFileParallel blockIn realSize (cryptTool : Cryptography.ICryptoTransform) =
        let encodeBlock = Array.zeroCreate (blockSize * cores)
        let asyncEncode = 
            // Вычисляем реальное количество ядер, которое нужно задействовать.
            let realCore = if realSize = blockSize * cores then cores else int (ceil <| (/) (float realSize) (float blockSize))
            let tasks  = seq {
                // Создаём по задаче на каждое задействованное ядро.
                for i in 1 .. realCore -> async {
                    // Проверяем непоследний ли сейчас блок нужно обработать...
                    if realSize - (blockSize * i) >= 0 then  
                        lock encodeBlock (fun _ -> cryptTool.TransformBlock(blockIn, (i-1)*blockSize, blockSize, encodeBlock, (i-1)*blockSize)) |> ignore
                        return blockSize
                    else
                        let count = abs <| blockSize * (i-1) - realSize
                        let result = cryptTool.TransformFinalBlock(blockIn, (i-1)*blockSize, count)
                        lock encodeBlock (fun _ -> Array.blit result 0 encodeBlock ((i-1)*blockSize) result.Length)
                        return result.Length
                }
            }
            Async.RunSynchronously(Async.Parallel tasks)

        let realTransformed = asyncEncode |> Array.sum
        Array.sub encodeBlock 0 realTransformed

    /// Основной процесс параллельного преобразования.
    let uni (blockIn : byte[]) realSize func =
        let encodeBlock = Array.zeroCreate (blockSize * cores)
        let asyncEncode = 
            // Вычисляем реальное количество ядер, которое нужно задействовать.
            let realCore = if realSize = blockSize * cores then cores else int (ceil <| (/) (float realSize) (float blockSize))
            let tasks  = seq {
                // Создаём по задаче на каждое задействованное ядро.
                for i in 1 .. realCore -> async {
                    // Проверяем непоследний ли сейчас блок нужно обработать...
                    if realSize - (blockSize * i) >= 0 then
                        lock encodeBlock (fun _ -> 
                                            let result = func(blockIn.[(i-1)*blockSize..i*blockSize-1], 16)
                                            Array.blit  result 0 encodeBlock ((i-1)*blockSize) result.Length
                                        )
                        return blockSize                    
                    else
                        lock encodeBlock (fun _ -> 
                                            let result = func(blockIn.[(i-1)*blockSize..], 16)
                                            Array.blit  result 0 encodeBlock ((i-1)*blockSize) result.Length
                                        )
                        return abs <| blockSize * (i-1) - realSize
                }
            }
            Async.RunSynchronously(Async.Parallel tasks)

        let realTransformed = asyncEncode |> Array.sum
        Array.sub encodeBlock 0 realTransformed

    member self.Log
        with set value = log <- value

    member self.Status
        with get ()    = status

    interface ICryptableClient with
        member self.StartTask key =
            if not isBusy then
                cipherKey <- key
                isBusy <- true
                aes <- initialAES key
                log "Начата новая задача."
        member self.EndTask () =
            if isBusy then
                isBusy <- false
                log "Задача завершена."

        member self.EncodeTask data realSize =
            let encryptor = aes.CreateEncryptor()
            uni data realSize encrypt

        member self.DecodeTask data realSize =
            let decryptor = aes.CreateDecryptor()
            uni data realSize decrypt

        member self.ExitNotice () =
            status <- Stopped
