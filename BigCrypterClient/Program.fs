﻿open System
open System.ServiceModel
open System.Runtime.InteropServices

open ClientService
open CryptoServiceLib

type CtrlType =
    | CTRL_C_EVENT = 0
    | CTRL_BREAK_EVENT = 1
    | CTRL_CLOSE_EVENT = 2
    | CTRL_LOGOFF_EVENT = 5
    | CTRL_SHUTDOWN_EVENT = 6

type ExitHandler = delegate of CtrlType -> bool

[<DllImport("Kernel32")>]
extern bool SetConsoleCtrlHandler(ExitHandler handler, bool add)

[<EntryPoint>]
let main argv = 
    let service = new ClientService()
    let instanceContext = new InstanceContext(service)
    let factory = new DuplexChannelFactory<ICryptable>(instanceContext, "NetTcpBinding_IClientService")
    let proxy = factory.CreateChannel()
    try
        let guid = proxy.Init(Environment.ProcessorCount)

        let Handler (signal : CtrlType) =
            proxy.End guid
            true

        let handler = new ExitHandler(Handler)
        SetConsoleCtrlHandler(handler, true) |> ignore

        printfn "Служба запущена."

        while service.Status <> Stopped do ()

        //proxy.End guid
        printfn "Служба остановлена."
    with
        | :? CommunicationException as ex -> printfn "%s" ex.Message
    Console.ReadKey() |> ignore
    0 // return an integer exit code
