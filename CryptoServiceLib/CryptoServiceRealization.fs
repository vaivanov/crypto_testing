﻿namespace CryptoServiceLib

open System
open System.IO
open System.ServiceModel
open System.Collections.Generic
open System.Threading.Tasks
open System.Security

/// Сведения о клиенте.
type private Client =
    {CPUCount : int; Contract : ICryptableClient; Index : int}

type CryptoEventArgs(text : string) =
    inherit EventArgs()
    
    member this.Text = text

type LogDelegate = delegate of obj * CryptoEventArgs -> unit

/// Переопредляем параметр события
type CryptoEventArgs2(indexBlock : int) =
    inherit EventArgs()    
    member this.indexBlock = indexBlock

/// Делегат - обработчик события
type endDelegate = delegate of obj * CryptoEventArgs2 -> unit


/// Реализация интерфейса криптослужбы.
[<ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)>] 
type CryptoService() =
    /// Клиенты-машины доступные для использования.
    let Clients = new Dictionary<Guid, Client>()
    /// Размер блока данных для обработки на одном ядреклиента.
    let BlockSize = 10*1024*1024

    let log = new Event<LogDelegate, CryptoEventArgs>() 
    
    let event = new Event<endDelegate, CryptoEventArgs2>()  
    
    let mutable globalIndex = 0

    [<CLIEvent>]
    member this.LogEvent = log.Publish
    [<CLIEvent>]
    member this.EndEvent = event.Publish

    member this.ClientsCount
        with get() = Clients.Count


    interface ICryptable with
        member this.Init cpuCount = 
            let contract = OperationContext.Current.GetCallbackChannel<ICryptableClient>()            
            let client = {CPUCount = cpuCount; Contract = contract; Index = globalIndex}
            globalIndex <- globalIndex + 1
            let guid = Guid.NewGuid()
            Clients.Add(guid, client)
            log.Trigger(this, new CryptoEventArgs("Подключился клиент с GUID = " + guid.ToString()))
            guid
        member this.End guid =
            globalIndex <- globalIndex - 1
            Clients.Remove guid |> ignore
            log.Trigger(this, new CryptoEventArgs("Отключился клиент с GUID = " + guid.ToString()))

    /// Шифрует указанный файл.
    member this.EncodeFile (fileIn : FileInfo) (fileOut : FileInfo) key =
        log.Trigger(this, new CryptoEventArgs "Начато шифрование файла.")
        use fileInStream = fileIn.OpenRead()
        use fileOutStream = fileOut.OpenWrite()
        let totalCPUs = Seq.fold (fun acc item -> acc+item.CPUCount) 0 Clients.Values
        let blockIn = Array.zeroCreate (totalCPUs*BlockSize)
        let blockOut = Array.zeroCreate (totalCPUs*BlockSize)

        // Генерируем коллекцию рабочих потоков.
        // Проходя по каждому клиенту, используем кол-во его ядер для работы с определённой областью
        // массивов blockIn и blockOut. При этом общение с каждым клиентом происходит асинхронно. (ибо let!)
        let wfs realRead = Clients.Values |> Seq.mapi (
                            fun index client -> async {
                                let fullBlockSize = BlockSize*client.CPUCount
                                let toSendLeft = realRead - index*fullBlockSize
                                if toSendLeft > 0 then
                                    let toSendReal = if toSendLeft > fullBlockSize then fullBlockSize else toSendLeft
                                    let result = client.Contract.EncodeTask (blockIn.[index*BlockSize*client.CPUCount..(index+1)*BlockSize*client.CPUCount-1]) toSendReal
                                    event.Trigger(this, new CryptoEventArgs2(client.Index))
                                    lock blockOut (fun _ -> Array.blit result 0 blockOut (index*BlockSize) result.Length)
                                    return result.Length
                                else
                                    return 0
                            }
            )
        
        // Уведомляем всех клиентов о начале новой задачи и раздаём ключ.
        Seq.iter (fun client -> client.Contract.StartTask key) Clients.Values
        log.Trigger(this, new CryptoEventArgs "Клиенты проинформированы о новой задаче. Шифруем...")
        // Считываем из файла до посинения...
        while fileInStream.Length - fileInStream.Position <> 0L do
            let realRead = fileInStream.Read(blockIn, 0, totalCPUs*BlockSize)
            let realTransformed = wfs realRead |> Async.Parallel |> Async.RunSynchronously |> Array.sum
            fileOutStream.Write(blockOut, 0, realTransformed)            

        // Уведомляем всех клиентов об окончании текущей задачи.
        Seq.iter (fun client -> client.Contract.EndTask ()) Clients.Values
        log.Trigger(this, new CryptoEventArgs "... завершено.")

    /// Дешифрует указанный файл.
    member this.DecodeFile (fileIn : FileInfo) (fileOut : FileInfo) key =
        log.Trigger(this, new CryptoEventArgs "Начато дешифрование файла.")
        use fileInStream = fileIn.OpenRead()
        use fileOutStream = fileOut.OpenWrite()
        let totalCPUs = Seq.fold (fun acc item -> acc+item.CPUCount) 0 Clients.Values
        let blockIn = Array.zeroCreate (totalCPUs*BlockSize)
        let blockOut = Array.zeroCreate (totalCPUs*BlockSize)

        // Генерируем коллекцию рабочих потоков.
        // Проходя по каждому клиенту, используем кол-во его ядер для работы с определённой областью
        // массивов blockIn и blockOut. При этом общение с каждым клиентом происходит асинхронно. (ибо let!)
        let wfs realRead = Clients.Values |> Seq.mapi (
                            fun index client -> async {
                                let fullBlockSize = BlockSize*client.CPUCount
                                let toSendLeft = realRead - index*fullBlockSize
                                if toSendLeft > 0 then
                                    let toSendReal = if toSendLeft > fullBlockSize then fullBlockSize else toSendLeft
                                    let result = client.Contract.DecodeTask (blockIn.[index*BlockSize*client.CPUCount..(index+1)*BlockSize*client.CPUCount-1]) toSendReal
                                    event.Trigger(this, new CryptoEventArgs2(client.Index))
                                    lock blockOut (fun _ -> Array.blit result 0 blockOut (index*BlockSize) result.Length)
                                    return result.Length
                                else
                                    return 0
                            }
            )
        
        // Уведомляем всех клиентов о начале новой задачи и раздаём ключ.
        Seq.iter (fun client -> client.Contract.StartTask key) Clients.Values
        log.Trigger(this, new CryptoEventArgs "Клиенты проинформированы о новой задаче. Дешифруем...")
        // Считываем из файла до посинения...
        while fileInStream.Length - fileInStream.Position <> 0L do
            let realRead = fileInStream.Read(blockIn, 0, totalCPUs*BlockSize)
            let realTransformed = wfs realRead |> Async.Parallel |> Async.RunSynchronously |> Array.sum
            fileOutStream.Write(blockOut, 0, realTransformed)

        // Уведомляем всех клиентов об окончании текущей задачи.
        Seq.iter (fun client -> client.Contract.EndTask ()) Clients.Values
        log.Trigger(this, new CryptoEventArgs "... завершено.")

    member this.StopService () =
        for client in Clients.Values do
            client.Contract.ExitNotice()
