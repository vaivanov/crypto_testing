﻿namespace CryptoServiceLib

open System
open System.ServiceModel
open System.Threading.Tasks

/// Интерфейс задач выполняемых клиентом.
[<ServiceContract>]
type ICryptableClient =
    /// Начинает новую задачу с указанным ID на стороне клиента.
    /// Для данной задачи передаётся секретный ключ.
    [<OperationContract>]
    abstract member StartTask : key:byte[] -> unit

    /// Завершает адачу с указанным ID на стороне клиента.
    [<OperationContract>]
    abstract member EndTask : unit -> unit

    /// Сервер посылает задачу с указанным ID клиенту:
    /// зашифровать блок байтов с указанным ключом.
    [<OperationContract>]
    abstract member EncodeTask : data:byte[] -> realSize:int -> byte[]

    /// Сервер посылает задачу с указанным ID клиенту:
    /// дешифровать блок байтов с указанным ключом.
    [<OperationContract>]
    abstract member DecodeTask : data:byte[] -> realSize:int -> byte[]

    /// Уведомляет клиента прекращении работы службы.
    [<OperationContract>]
    abstract member ExitNotice : unit -> unit

/// Интерфейс криптослужбы сервера.
[<ServiceContract(CallbackContract=typeof<ICryptableClient>, SessionMode=SessionMode.Required)>]
type ICryptable =
    /// Инициализирует работу клиента с сервером. Клиент сообщает о количестве
    /// ядер (процессоров), которые у него есть.
    [<OperationContract(IsInitiating=true)>]
    abstract member Init : CPUCount:int -> Guid

    /// Клиент уведомляет сервер о завершении своей работы.
    [<OperationContract(IsTerminating=true)>]
    abstract member End : clientId:Guid -> unit
